# Taller 1


Realizando interfaz grafica (UI) de un formulario sencillo.

Su objetivo principal era aprender a implementar un formulario sencillo hecho con html puro.

De este formulario, pude aprender a implementarle el fondo a una pagina web, 
de igual manera a agregar títulos, y subtítulos, así como los diferentes campos
para efectuar correctamente un formulario.
